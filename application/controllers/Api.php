<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    /**
     * Redirects the api calls to respective functions
     * @author Dinesh Paradesi Vadamodula
     * 
     */
    public function route($route){
        switch ($route){
            case 'student_login':
                $this->student_login();
                break;
            default:
                break;
        }
    }
    /**
     * Checks if the provided login details are valid
     * Sends back a success or fail notification
     * @author Dinesh Paradesi Vadamodula
     */
    private function student_login(){

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Expose-Headers: ETag');
        header('Content-Type: application/json');

        $student_id = $this->input->post('student_id');
        $password = $this->input->post('password');

        $is_login = $this->db->select('id, student_name')->get_where('students', ['student_id'=> $student_id, 'password'=>$password])->row();
        $is_login ? jsend_success(['result' => ['token' =>VERIFY_TOKEN, 'student_name' => $is_login->student_name]]) : jsend_fail();
    }
}
