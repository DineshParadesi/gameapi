<?php
/**
 * @param array $data: Acts as the wrapper for any data returned by the API call. If the call returns no data (as in the last example), data should be set to null.
 */

function jsend_success($data = null){

	echo json_encode([
		'status' => 'success',
		'data' => $data
	]);

}

/**
 * @param array $data: Acts as the wrapper for any data returned by the API call. If the call returns no data (as in the last example), data should be set to null.
 */
function jsend_fail($data = null){
	echo json_encode([
		'status' => 'fail',
		'data' => $data
	]);
	
}
