<?php

define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
define('BASE_URL','');
define('MEDIA_URL','');
define('TITLE','ACTAC');
define('ADMIN_SESS','');
define('TESTING_EMAILS',true);
define('TESTING_EMAIL','');
define('DEVELOPMENT',TRUE);
define('LMS_KEY','');
define('EQ_API','');
define('TIR_API_URL','');
define('TIR_API_AUTH_KEY','');
define('RTOS',[]);
define("TIR_API", "");
